package com.module.admin.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.module.admin.sys.pojo.SysUserAtt;

/**
 * sys_user_att的Dao
 * @author autoCode
 * @date 2018-06-12 09:59:57
 * @version V1.0.0
 */
public interface SysUserAttDao {

	public abstract void save(SysUserAtt sysUserAtt);

	public abstract void update(SysUserAtt sysUserAtt);

	public abstract void delete(@Param("userId")Integer userId, @Param("type")String type, @Param("typeNo")String typeNo);

	public abstract SysUserAtt get(@Param("userId")Integer userId, @Param("type")String type, @Param("typeNo")String typeNo);

	public abstract List<SysUserAtt> findSysUserAtt(SysUserAtt sysUserAtt);
	
	public abstract int findSysUserAttCount(SysUserAtt sysUserAtt);
}