package com.module.admin.prj.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.prj.pojo.PrjOptimizeLog;
import com.module.admin.prj.service.PrjInfoService;
import com.module.admin.prj.service.PrjOptimizeLogService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 系统优化记录的Controller
 * @author yuejing
 * @date 2016-11-30 13:30:00
 * @version V1.0.0
 */
@Controller
public class PrjOptimizeLogController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrjOptimizeLogController.class);

	@Autowired
	private PrjOptimizeLogService prjOptimizeLogService;
	@Autowired
	private PrjInfoService prjInfoService;

	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/prjOptimizeLog/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/optimizeLog-manager";
	}
	
	@RequestMapping(value = "/prjOptimizeLog/f-view/look")
	public String look(HttpServletRequest request, ModelMap modelMap,
			Integer id) {
		PrjOptimizeLog prjOptimizeLog = prjOptimizeLogService.getDtl(id);
		modelMap.put("prjOptimizeLog", prjOptimizeLog);
		return "admin/prj/optimizeLog-look";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/prjOptimizeLog/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			PrjOptimizeLog prjOptimizeLog) {
		ResponseFrame frame = null;
		try {
			frame = prjOptimizeLogService.pageQuery(prjOptimizeLog);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/prjOptimizeLog/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response,
			Integer id) {
		ResponseFrame frame = null;
		try {
			frame = prjOptimizeLogService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/prjOptimizeLog/f-json/deleteAll")
	@ResponseBody
	public void deleteAll(HttpServletRequest request, HttpServletResponse response) {
		ResponseFrame frame = null;
		try {
			frame = prjOptimizeLogService.deleteAll();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}