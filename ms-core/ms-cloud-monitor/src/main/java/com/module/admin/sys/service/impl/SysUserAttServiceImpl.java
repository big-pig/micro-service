package com.module.admin.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.sys.dao.SysUserAttDao;
import com.module.admin.sys.pojo.SysUserAtt;
import com.module.admin.sys.service.SysUserAttService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * sys_user_att的Service
 * @author autoCode
 * @date 2018-06-12 09:59:57
 * @version V1.0.0
 */
@Component
public class SysUserAttServiceImpl implements SysUserAttService {

	@Autowired
	private SysUserAttDao sysUserAttDao;
	
	@Override
	public ResponseFrame save(SysUserAtt sysUserAtt) {
		ResponseFrame frame = new ResponseFrame();
		SysUserAtt org = get(sysUserAtt.getUserId(), sysUserAtt.getType(), sysUserAtt.getTypeNo());
		if(org == null) {
			sysUserAttDao.save(sysUserAtt);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public SysUserAtt get(Integer userId, String type, String typeNo) {
		return sysUserAttDao.get(userId, type, typeNo);
	}

	@Override
	public ResponseFrame pageQuery(SysUserAtt sysUserAtt) {
		sysUserAtt.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = sysUserAttDao.findSysUserAttCount(sysUserAtt);
		List<SysUserAtt> data = null;
		if(total > 0) {
			data = sysUserAttDao.findSysUserAtt(sysUserAtt);
		}
		Page<SysUserAtt> page = new Page<SysUserAtt>(sysUserAtt.getPage(), sysUserAtt.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(Integer userId, String type, String typeNo) {
		ResponseFrame frame = new ResponseFrame();
		sysUserAttDao.delete(userId, type, typeNo);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
}