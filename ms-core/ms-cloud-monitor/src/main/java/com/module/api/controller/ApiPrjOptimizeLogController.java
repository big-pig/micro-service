package com.module.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.module.api.service.ApiPrjOptimizeLogService;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 系统优化记录的接口
 * @author 岳静
 * @date 2016年3月4日 下午6:22:39 
 * @version V1.0
 */
@RestController
public class ApiPrjOptimizeLogController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiPrjOptimizeLogController.class);
	
	@Autowired
	private ApiPrjOptimizeLogService apiPrjOptimizeLogService;

	/**
	 * 保存记录
	 */
	@RequestMapping(value = "/api/prjOptimizeLog/save")
	@ResponseBody
	public ResponseFrame save(String code, String url, String params,
			String reqTime, String resTime, Integer useTime, String resResult, String remark) {
		ResponseFrame frame = null;
		try {
			frame = apiPrjOptimizeLogService.save(code, url, params, FrameTimeUtil.parseDate(reqTime), FrameTimeUtil.parseDate(resTime), useTime, resResult, remark);
		} catch (Exception e) {
			LOGGER.error("获取使用的密钥异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		return frame;
	}
}